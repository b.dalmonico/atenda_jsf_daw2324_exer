package backing;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import modelo.LineaPedido;
import modelo.ModeloDAO;
import modelo.Pedido;
import modelo.Produto;

@Named
@SessionScoped
public class UserBacking implements Serializable {
	private static final Logger logger= Logger.getLogger(UserBacking.class.getName());
	private static final long serialVersionUID = 1L;

	@Inject
	ModeloDAO modeloDAO;
	@Inject
	Credenciais credenciais;

	boolean carroBaleiro = true;
	Pedido carro = new Pedido();
	ArrayList<LineaPedido> lineasPedidoCatalogo;
	ArrayList<Pedido> pedidos;
	ArrayList<Pedido> pedidosPendentesRecepcion;	
	Pedido pedidoAberto = new Pedido();
	boolean detallesPedido = false;

	
	@PostConstruct
	public void init() {
		if (credenciais.isAutenticado()) {
			cargaPedidosPendentesRecepcion();
			cargaCarro();
		}
		cargaCatalogo();
	}

	public void recibePedido(Pedido pedido) {
		FacesContext context = FacesContext.getCurrentInstance();
		pedido.setRecibido(true);
		try {
			modeloDAO.actualiza(pedido);
			context.addMessage("perfil",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Pedido recibido con éxito"));
			cargaPedidos();
		} catch (Exception e) {
			e.printStackTrace();
		}
		cargaPedidosPendentesRecepcion();
	}

	public void cargaPedidosPendentesRecepcion() {
		// nos últimos 6 meses
		boolean eDevolucion = false;
		boolean pechado = true;
		boolean recibido = false;
		try {
			pedidosPendentesRecepcion = modeloDAO.getPedidosPeriodoDe(LocalDate.now().minusMonths(6), LocalDate.now(),
					eDevolucion, credenciais.getUsuarioLogueado(), pechado, recibido);
			logger.info("userBacking::pedidosPendentesRecepcion: " + pedidosPendentesRecepcion);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void detallesPedido(int idPedido) {
		//System.out.println("detallesPedido: " + idPedido);
		if (credenciais.isAutenticado()) {
			try {
				//carroDevolucion = null;
				pedidoAberto = modeloDAO.getPedidoPorId(idPedido);
				detallesPedido = true;
				//System.out.println("detallesPedido: " + pedidoAberto);
			} catch (Exception e) {
			}
		}
	}	
	public String segueMercando() {
		cargaPedidosPendentesRecepcion();
		cargaCatalogo();
		return "index";
	}

	public void cargaCatalogo() {
		lineasPedidoCatalogo = new ArrayList<LineaPedido>();
		try {
			ArrayList<Produto> produtos = modeloDAO.getAll();
			for (Produto produto : produtos) {
				if (produto.getStock() > 0) { // so engade produto se hai stock positivo
					LineaPedido lineaPedido = new LineaPedido();
					lineaPedido.setProduto(produto);
					//System.out.println("cargaCatalogo: produto: " + produto.getNome() + " stock: " + produto.getStock());
					lineaPedido.setUnidades(0);
					lineaPedido.setDesconto(0);
					lineasPedidoCatalogo.add(lineaPedido);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void add(LineaPedido lineaPedido) {
		if (lineaPedido.getUnidades() > 0) {
			// usuario logueado => gardamos na BD
			if (carroBaleiro) {
				carro.setData(LocalDateTime.now()); // data provisional de cando se engade a primeira lineaPedido
				carro.setCliente(credenciais.getUsuarioLogueado()); // asociar o carro ao cliente logueado (ANON ou
															// outro)
				carro.setIdPedidoOrix(0); // 0 equivale a NULL ao insertar na BD
				carroBaleiro = false;
			}
			// si está usuarioLogueado, metemos carro en base de datos
			if (credenciais.isAutenticado()) {
					try {
						if (carro.getId() == 0) {
							carro.setId(modeloDAO.inserta(carro));
						} else {
							modeloDAO.actualiza(carro);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}				
			}
			// actualizo stock de produto na memoria. Na BD só cando se pida
			lineaPedido.getProduto().setStock(lineaPedido.getProduto().getStock() - lineaPedido.getUnidades());
			// System.out.println("engado a carro: " + lineaPedido);
			boolean produtoEnCarro = false;
			for (LineaPedido lineaCarro : carro.getLineasPedido()) {
				// si produto en carro incremento unidades
				if (lineaCarro.getProduto().equals(lineaPedido.getProduto())) {
					produtoEnCarro = true;
					// actualiza unidades en lineaCarro
					lineaCarro.setUnidades(lineaCarro.getUnidades() + lineaPedido.getUnidades());
					if (credenciais.isAutenticado()) {
						try {
							modeloDAO.actualiza(lineaCarro);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
			if (!produtoEnCarro) {
				// se produto non en carro meto en carro nova linea pedido
				LineaPedido lineaCarro = new LineaPedido();
				lineaCarro.setProduto(lineaPedido.getProduto());
				lineaCarro.setUnidades(lineaPedido.getUnidades());
				lineaCarro.setPrezo(lineaPedido.getProduto().getPrezo());
				lineaCarro.setCoste(lineaPedido.getProduto().getCoste());
				// lineaCarro.setDesconto(lineaPedido.getDesconto());
				carro.addLineaPedido(lineaCarro);
				if (credenciais.isAutenticado()) {
					try {
						lineaCarro.setId(modeloDAO.inserta(lineaCarro, carro));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			// poño a cero as unidaes da lineaPedido do catálogo de produtos
			lineaPedido.setUnidades(0);
		}
	}

	public String pide() {
		FacesContext context = FacesContext.getCurrentInstance();
		limpaCarro();
		// insertar pedido en BD se está autenticado
		if (credenciais.isAutenticado()) {
			try {
				// ajustar pedido a disponibilidad real de produtos en BD			
				ListIterator<LineaPedido> it = carro.getLineasPedido().listIterator();
				while (it.hasNext()) {
					LineaPedido lineaCarro = (LineaPedido) it.next();
					Produto produtoBD = modeloDAO.getProdutoPorId(lineaCarro.getProduto().getId());
					int stockBD = produtoBD.getStock();
					if (stockBD < lineaCarro.getUnidades() && produtoBD.getStock() > 0) {
						System.out.println(
								"userBacking:pide: non hai produtos bastantes. quedan: " + produtoBD.getStock());
						// mensaxe de produto agotado
						context.addMessage("carro", new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Produto '"
								+ produtoBD.getNome() + "' agotado. Só quedan " + produtoBD.getStock() + " unidades"));
						// actualizo carro reducindo unidades
						lineaCarro.setUnidades(produtoBD.getStock());
						return "carro";
					} else if (stockBD == 0) {
						System.out.println("userBacking:pide:produto agotado. quedan: " + produtoBD.getStock());
						context.addMessage("carro", new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso",
								"Produto '" + produtoBD.getNome() + "' agotado"));
						it.remove();
						return "carro";
					} else {
						lineaCarro.getProduto().setStock(stockBD-lineaCarro.getUnidades());
						produtoBD.setStock(stockBD-lineaCarro.getUnidades());
					}
					modeloDAO.actualiza(produtoBD);								
				}
				limpaCarro();
				if (!carroBaleiro) {
					Pedido carroBD = modeloDAO.getPedidoPorId(carro.getId());
					if (carroBD != null && carroBD.getId() == carro.getId()) {
						// se XA está na base de datos ///
						// pecho carro como pedido na BD....
						//////////////////////////////////////////
						System.out.println("pide() ::: carro xa na bd: " + carro);
						carro.setData(LocalDateTime.now()); // Actualizo data ao momento de pedir
						carro.setCliente(credenciais.getUsuarioLogueado()); // asocio cliente por se acaba de loguearse
						carro.setPechado(true);
						modeloDAO.actualiza(carro);
						System.out.println("pide() ::: carro actualizado: "+carro);
						
					} else {
						// se non está na base de datos
						///////////////////////////
						System.out.println("carro non está na bd: ");
						Pedido pedido = new Pedido();
						pedido.setData(LocalDateTime.now());
						pedido.setCliente(credenciais.getUsuarioLogueado()); // asociar o pedido ao cliente logueado
						pedido.setPechado(true);
						pedido.setId(modeloDAO.inserta(pedido));
						for (LineaPedido lineaCarro : carro.getLineasPedido()) {
							lineaCarro.setId(modeloDAO.inserta(lineaCarro, pedido));
							modeloDAO.actualiza(lineaCarro.getProduto());
							pedido.addLineaPedido(lineaCarro);
						}
					}
					context.addMessage("carro",
							new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Pedido tramitado con éxito"));
					context.getExternalContext().getFlash().setKeepMessages(true);
				}
				// borrar carro
				carro = new Pedido();
				carroBaleiro = true;
				limpaCarro();
				// actualizo pedidos a recibir, catalogo e carro
				init(); 
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "index"; // volvo ao index
		} else {
			// debe autenticarse para tramitar pedido
			context.addMessage("carro",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Debe loguearse para mercar"));
			// return "carro"; // quedo en carro
		}
		return "carro";
	}

	public void cargaCarro() {
		// para usuario logueado
				if (credenciais.isAutenticado()) {
					try {
						// miro se hai pedido aberto na base de datos
						boolean eDevolucion = false;
						boolean pechado = false;
						boolean recibido = false;
						ArrayList<Pedido> pedidosAbertosUsuario = modeloDAO.getPedidosPeriodoDe(LocalDate.now().minusDays(5),
								LocalDate.now(), eDevolucion, credenciais.getUsuarioLogueado(), pechado, recibido);
						if (pedidosAbertosUsuario != null && !pedidosAbertosUsuario.isEmpty()) {
							// hai pedido aberto na base de datos
							if (carroBaleiro) {
								// se carro baleiro cargoo co da base de datos
								carro = pedidosAbertosUsuario.get(0);
								System.out.println("UserBacking; cargaCarro: cargo da BD;" + carro);
								carroBaleiro = false;
							} else {
								// se carro non baleiro, borro o da base de datos
								// e gardo o carro na base de datos.
								modeloDAO.borra(pedidosAbertosUsuario.get(0));
								System.out.println("UserBacking; cargaCarro: borro da  BD;" + pedidosAbertosUsuario.get(0));

								carro.setCliente(credenciais.getUsuarioLogueado());
								carro.setId(modeloDAO.inserta(carro));
								System.out.println("UserBacking; cargaCarro: inserto en BD;" + carro);
							}
						} else if (pedidosAbertosUsuario == null || pedidosAbertosUsuario.isEmpty()) {
							if (!carroBaleiro) {
								carro.setCliente(credenciais.getUsuarioLogueado());
								modeloDAO.inserta(carro);
								System.out.println("userbacking:cargaCarro:: inserto carro con id:: " + carro.getId());
								carro.setId(carro.getId());
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
	}

	public String cargaPedidos() {
		if (credenciais.isAutenticado()) {
			// limpo pedidos abertos
			pedidoAberto = null;
			detallesPedido = false;
			// actualizo lista de pedidos do usuario
			// pedir pedidos sen as devolucións			
			try {
				boolean eDevolucion = false;
				boolean pechado = true;
				boolean recibido = true;
				pedidos = modeloDAO.getPedidosPeriodoDe(LocalDate.now().minusYears(2), LocalDate.now(), eDevolucion,
						credenciais.getUsuarioLogueado(), pechado, recibido);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "pedidos";
	}

	public int getNumArticulosCarro() {
		int numArticulos = 0;
		for (LineaPedido linea : carro.getLineasPedido()) {
			numArticulos += linea.getUnidades();
		}
		return numArticulos;
	}

	public String showCarro() {
		limpaCarro();
		cargaCatalogo();
		carroBaleiro = carro.getLineasPedido().isEmpty();
		if (carroBaleiro) {
			return null;
		} else {
			return "carro";
		}
	}
	
	private void limpaCarro() {
		// limpiar carro de prods con unidades =0
		Iterator<LineaPedido> it = carro.getLineasPedido().iterator();
		while (it.hasNext()) {
			LineaPedido linea = it.next();
			if (linea.getUnidades() == 0) {
				it.remove();
			}
		}
		if (carro.getLineasPedido().size() == 0) {
			this.carroBaleiro = true;
		}
	}	
	
	public void updateCarro(ValueChangeEvent event) {
		System.out.println("userBacking:updateCarro:: carro:: " + carro);
		// O evento producirase antes do cambio de valor. Entón agardamos á fase de
		// INVOKE_APPLICATION que está despois de UPDATE_MODEL_VALUES
		// para acceder aos datos do modelo da "forma usual"
		if (event.getPhaseId() != PhaseId.INVOKE_APPLICATION) {
			event.setPhaseId(PhaseId.INVOKE_APPLICATION);
			event.queue();
			return;
		}
		if (credenciais.isAutenticado()) {// se logueado actualizamos carro na BD
			// actualizamos o carro na BD cos valores actualizados que ten no modelo
			try {
				if (!carroBaleiro) {
					for (LineaPedido linea : carro.getLineasPedido()) {
						modeloDAO.actualiza(linea);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public String eliminaDoCarro(LineaPedido lineaCarro) {
		carro.removeLineaPedido(lineaCarro);
		limpaCarro();
		if (credenciais.isAutenticado()) { // carro na BD
			if (carroBaleiro) {
				try {
					modeloDAO.borra(carro);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return "index";
			} else {
				try {
					modeloDAO.borra(lineaCarro);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return "";
			}
		} else {
			return "";
		}

	}
	
	///////////////////////////////////////7
	public Pedido getPedidoAberto() {
		return pedidoAberto;
	}

	public void setPedidoAberto(Pedido pedidoAberto) {
		this.pedidoAberto = pedidoAberto;
	}

	public boolean isDetallesPedido() {
		return detallesPedido;
	}

	public void setDetallesPedido(boolean detallesPedido) {
		this.detallesPedido = detallesPedido;
	}

	public Pedido getCarro() {
		return carro;
	}

	public void setCarro(Pedido carro) {
		this.carro = carro;
	}

	public ArrayList<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(ArrayList<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public boolean isCarroBaleiro() {
		return carroBaleiro;
	}

	public void setCarroBaleiro(boolean carroBaleiro) {
		this.carroBaleiro = carroBaleiro;
	}

	public ArrayList<LineaPedido> getLineasPedidoCatalogo() {
		return lineasPedidoCatalogo;
	}

	public void setLineasPedidoCatalogo(ArrayList<LineaPedido> lineasPedido) {
		this.lineasPedidoCatalogo = lineasPedido;
	}

	public ArrayList<Pedido> getPedidosPendentesRecepcion() {
		return pedidosPendentesRecepcion;
	}

	public void setPedidosPendentesRecepcion(ArrayList<Pedido> pedidosPendentesRecepcion) {
		this.pedidosPendentesRecepcion = pedidosPendentesRecepcion;
	}

}
